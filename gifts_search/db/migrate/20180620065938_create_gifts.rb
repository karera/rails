class CreateGifts < ActiveRecord::Migration[5.2]
  def change
    create_table :gifts do |t|
      t.text :title
      t.binary :photo
      t.text :url
      t.float :latitude
      t.float :longitude

      t.timestamps
    end
  end
end
