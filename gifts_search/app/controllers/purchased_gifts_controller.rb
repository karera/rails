class PurchasedGiftsController < ApplicationController
  before_action :set_purchased_gift, only: [:show, :edit, :update, :destroy]

  # GET /purchased_gifts
  # GET /purchased_gifts.json
  def index
    @purchased_gifts = PurchasedGift.all
  end

  # GET /purchased_gifts/1
  # GET /purchased_gifts/1.json
  def show
  end

  # GET /purchased_gifts/new
  def new
    @purchased_gift = PurchasedGift.new
  end

  # GET /purchased_gifts/1/edit
  def edit
  end

  # POST /purchased_gifts
  # POST /purchased_gifts.json
  def create
    @purchased_gift = PurchasedGift.new(purchased_gift_params)

    respond_to do |format|
      if @purchased_gift.save
        format.html { redirect_to @purchased_gift, notice: 'Purchased gift was successfully created.' }
        format.json { render :show, status: :created, location: @purchased_gift }
      else
        format.html { render :new }
        format.json { render json: @purchased_gift.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /purchased_gifts/1
  # PATCH/PUT /purchased_gifts/1.json
  def update
    respond_to do |format|
      if @purchased_gift.update(purchased_gift_params)
        format.html { redirect_to @purchased_gift, notice: 'Purchased gift was successfully updated.' }
        format.json { render :show, status: :ok, location: @purchased_gift }
      else
        format.html { render :edit }
        format.json { render json: @purchased_gift.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /purchased_gifts/1
  # DELETE /purchased_gifts/1.json
  def destroy
    @purchased_gift.destroy
    respond_to do |format|
      format.html { redirect_to purchased_gifts_url, notice: 'Purchased gift was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_purchased_gift
      @purchased_gift = PurchasedGift.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def purchased_gift_params
      params.require(:purchased_gift).permit(:user_id, :gift_id, :memo)
    end
end
