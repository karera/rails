Rails.application.routes.draw do
  resources :purchased_gifts
  resources :favorites
  resources :users
  resources :gifts
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
