json.extract! purchased_gift, :id, :user_id, :gift_id, :memo, :created_at, :updated_at
json.url purchased_gift_url(purchased_gift, format: :json)
