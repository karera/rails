require 'test_helper'

class PurchasedGiftsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @purchased_gift = purchased_gifts(:one)
  end

  test "should get index" do
    get purchased_gifts_url
    assert_response :success
  end

  test "should get new" do
    get new_purchased_gift_url
    assert_response :success
  end

  test "should create purchased_gift" do
    assert_difference('PurchasedGift.count') do
      post purchased_gifts_url, params: { purchased_gift: { gift_id: @purchased_gift.gift_id, memo: @purchased_gift.memo, user_id: @purchased_gift.user_id } }
    end

    assert_redirected_to purchased_gift_url(PurchasedGift.last)
  end

  test "should show purchased_gift" do
    get purchased_gift_url(@purchased_gift)
    assert_response :success
  end

  test "should get edit" do
    get edit_purchased_gift_url(@purchased_gift)
    assert_response :success
  end

  test "should update purchased_gift" do
    patch purchased_gift_url(@purchased_gift), params: { purchased_gift: { gift_id: @purchased_gift.gift_id, memo: @purchased_gift.memo, user_id: @purchased_gift.user_id } }
    assert_redirected_to purchased_gift_url(@purchased_gift)
  end

  test "should destroy purchased_gift" do
    assert_difference('PurchasedGift.count', -1) do
      delete purchased_gift_url(@purchased_gift)
    end

    assert_redirected_to purchased_gifts_url
  end
end
