class CreateFavorites < ActiveRecord::Migration[5.2]
  def change
    create_table :favorites do |t|
      t.integer :user_id
      t.integer :gift_id
      t.text :memo

      t.timestamps
    end
  end
end
