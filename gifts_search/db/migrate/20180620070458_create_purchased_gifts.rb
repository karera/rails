class CreatePurchasedGifts < ActiveRecord::Migration[5.2]
  def change
    create_table :purchased_gifts do |t|
      t.integer :user_id
      t.integer :gift_id
      t.text :memo

      t.timestamps
    end
  end
end
