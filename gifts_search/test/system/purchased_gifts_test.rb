require "application_system_test_case"

class PurchasedGiftsTest < ApplicationSystemTestCase
  setup do
    @purchased_gift = purchased_gifts(:one)
  end

  test "visiting the index" do
    visit purchased_gifts_url
    assert_selector "h1", text: "Purchased Gifts"
  end

  test "creating a Purchased gift" do
    visit purchased_gifts_url
    click_on "New Purchased Gift"

    fill_in "Gift", with: @purchased_gift.gift_id
    fill_in "Memo", with: @purchased_gift.memo
    fill_in "User", with: @purchased_gift.user_id
    click_on "Create Purchased gift"

    assert_text "Purchased gift was successfully created"
    click_on "Back"
  end

  test "updating a Purchased gift" do
    visit purchased_gifts_url
    click_on "Edit", match: :first

    fill_in "Gift", with: @purchased_gift.gift_id
    fill_in "Memo", with: @purchased_gift.memo
    fill_in "User", with: @purchased_gift.user_id
    click_on "Update Purchased gift"

    assert_text "Purchased gift was successfully updated"
    click_on "Back"
  end

  test "destroying a Purchased gift" do
    visit purchased_gifts_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Purchased gift was successfully destroyed"
  end
end
