json.extract! gift, :id, :title, :photo, :url, :latitude, :longitude, :created_at, :updated_at
json.url gift_url(gift, format: :json)
