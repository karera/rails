json.extract! favorite, :id, :user_id, :gift_id, :memo, :created_at, :updated_at
json.url favorite_url(favorite, format: :json)
